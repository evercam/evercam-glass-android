Evercam for Glass
=============

The Evercam for Glass integrates with Evercam API (/public/nearest.jpg) to explore CCTV cameras nearby. It will show a snapshot of the nearest camera when requested. 

Evercam Glassware is now live in the [MyGlass site](https://glass.google.com/u/0/glassware/4238721003195886173).
