package glass.android.evercam.io.evercamglass;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.Window;

import com.google.android.glass.media.Sounds;
import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;
import com.google.android.glass.widget.CardBuilder;


public abstract class AlertActivity extends Activity
{
    private final String TAG = "evercamglass-AlertActivity";
    protected GestureDetector mGestureDetector;

    @Override
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);

        AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audio.playSoundEffect(Sounds.ERROR);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(buildAlertCard(this).getView());

        mGestureDetector = createGestureDetector(this);

    }

    protected GestureDetector createGestureDetector(Context context)
    {
        GestureDetector gestureDetector = new GestureDetector(context);
        //Create a base listener for generic gestures
        gestureDetector.setBaseListener(new GestureDetector.BaseListener()
        {
            @Override
            public boolean onGesture(Gesture gesture)
            {
                if(gesture == Gesture.TAP)
                {
                    onGestureTapped();

                    return true;
                }
                return false;
            }
        });
        return gestureDetector;
    }

    /*
     * Send generic motion events to the gesture detector
     */
    @Override
    public boolean onGenericMotionEvent(MotionEvent event)
    {
        if(mGestureDetector != null)
        {
            return mGestureDetector.onMotionEvent(event);
        }
        return false;
    }

    abstract void onGestureTapped();

    abstract CardBuilder buildAlertCard(Context context);
}
