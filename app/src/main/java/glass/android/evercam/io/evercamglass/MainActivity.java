package glass.android.evercam.io.evercamglass;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.glass.media.Sounds;
import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;

import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import io.evercam.EvercamException;
import io.evercam.PublicCamera;

public class MainActivity extends Activity implements LocationListener
{
    private final String TAG = "evercamglass-MainActivity";
    private ImageView imageView;
    private ImageView loadingLogoImageView;
    private com.google.glass.widget.SliderView sliderView;
    private RelativeLayout progressLayout;
    private GestureDetector mGestureDetector;
    private LocationManager mLocationManager;
    private Location realLocation;

    @Override
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(new TuggableView(this, R.layout.image_layout));

        mGestureDetector = createGestureDetector(this);
        imageView = (ImageView) findViewById(R.id.imageView);
        loadingLogoImageView = (ImageView) findViewById(R.id.loading_logo_imageview);
        sliderView = (com.google.glass.widget.SliderView) findViewById(R.id.indeterm_slider);
        progressLayout = (RelativeLayout) findViewById(R.id.progress_layout);
        sliderView.startIndeterminate();

        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                new MainCheckInternetTask(MainActivity.this).execute();
            }
        }, 1000);
    }

    private GestureDetector createGestureDetector(Context context)
    {
        GestureDetector gestureDetector = new GestureDetector(context);
        //Create a base listener for generic gestures
        gestureDetector.setBaseListener(new GestureDetector.BaseListener()
        {
            @Override
            public boolean onGesture(Gesture gesture)
            {
                if(gesture == Gesture.TAP)
                {
                    // do something on tap
                    Log.d(TAG, "Gesture.TAP");
                    AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                    audio.playSoundEffect(Sounds.DISALLOWED);
                    return true;
                }
                else if(gesture == Gesture.TWO_TAP)
                {
                    // do something on two finger tap
                    return true;
                }
                else if(gesture == Gesture.SWIPE_RIGHT)
                {
                    // do something on right (forward) swipe
                    return true;
                }
                else if(gesture == Gesture.SWIPE_LEFT)
                {
                    // do something on left (backwards) swipe
                    return true;
                }
                return false;
            }
        });
        return gestureDetector;
    }

    /*
     * Send generic motion events to the gesture detector
     */
    @Override
    public boolean onGenericMotionEvent(MotionEvent event)
    {
        if(mGestureDetector != null)
        {
            return mGestureDetector.onMotionEvent(event);
        }
        return false;
    }

    /**
     * Location Listener
     */
    @Override
    public void onLocationChanged(Location location)
    {
        Log.d(TAG, "Location changed");
        realLocation = location;
        Log.d(TAG, "New location" + realLocation.getLatitude() + "," + realLocation.getLongitude());
        mLocationManager.removeUpdates(this);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras)
    {

    }

    @Override
    public void onProviderEnabled(String provider)
    {

    }

    @Override
    public void onProviderDisabled(String provider)
    {

    }

    private Bitmap retrieveBitmapFromLocation(String locationString) throws EvercamException
    {
        InputStream stream = PublicCamera.getNearestJpgStream(locationString);
        return BitmapFactory.decodeStream(stream);
    }

    private class RequestNearestCameraTask extends AsyncTask<Void, Void, Bitmap>
    {
        @Override
        protected Bitmap doInBackground(Void... params)
        {
            try
            {
                if(realLocation != null)
                {
                    String locationString = realLocation.getLatitude() + "," +
                            "" + realLocation.getLongitude();
                    Log.d(TAG, "Requesting snapshot for location: " + locationString);
                    return retrieveBitmapFromLocation(locationString);
                }
                else
                {
                    Thread.sleep(2000);
                    if(realLocation != null)
                    {
                        String locationString = realLocation.getLatitude() + "," +
                                "" + realLocation.getLongitude();
                        Log.d(TAG, "Requesting snapshot for the second time on location: " +
                                locationString);
                        return retrieveBitmapFromLocation(locationString);
                    }
                }
                //If no location data returned, use IP address instead
                return retrieveBitmapFromLocation(null);
            }
            catch(EvercamException e)
            {
                Log.e(TAG, e.toString());
            }
            catch(InterruptedException e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute()
        {
            progressLayout.setVisibility(View.VISIBLE);
            loadingLogoImageView.setVisibility(View.VISIBLE);
            sliderView.startIndeterminate();
        }

        @Override
        protected void onPostExecute(Bitmap bitmap)
        {
            progressLayout.setVisibility(View.GONE);

            new Handler().postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    MainActivity.this.getWindow().clearFlags(WindowManager.LayoutParams
                            .FLAG_KEEP_SCREEN_ON);
                }
            }, 1);

            bitmap = null;
            if(bitmap != null)
            {
                sliderView.stopIndeterminate();
                progressLayout.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);
                imageView.setImageBitmap(bitmap);
            }

            else
            {
                Log.e(TAG, "Image is null");
                MainActivity.this.finish();
                startActivity(new Intent(MainActivity.this, NoCameraAlertActivity.class));
            }
        }
    }

    private class MainCheckInternetTask extends CheckInternetTask
    {
        public MainCheckInternetTask(Context context)
        {
            super(context);
        }

        @Override
        protected void onPreExecute()
        {
        }

        @Override
        protected void onPostExecute(Boolean hasNetwork)
        {
            if(hasNetwork)
            {
                Log.d(TAG, "Network connected");

                mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                List<String> providers = mLocationManager.getProviders(true);
                for(Iterator<String> i = providers.iterator(); i.hasNext(); )
                {
                    String nextProvider = i.next();
                    Log.d(TAG, "Provider:" + nextProvider);
                    mLocationManager.requestLocationUpdates(nextProvider, 0, 0,
                            MainActivity.this);
                }

                new RequestNearestCameraTask().execute();
            }
            else
            {
                Log.d(TAG, "Network not connected");
                finish();
                startActivity(new Intent(MainActivity.this, NoInternetAlertActivity.class));
            }
        }
    }
}
