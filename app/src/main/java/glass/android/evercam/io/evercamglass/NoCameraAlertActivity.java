package glass.android.evercam.io.evercamglass;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;

import com.google.android.glass.media.Sounds;
import com.google.android.glass.widget.CardBuilder;


public class NoCameraAlertActivity extends AlertActivity
{
    private final String TAG = "evercamglass-NoCameraAlertActivity";

    @Override
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    @Override
    void onGestureTapped()
    {
        AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audio.playSoundEffect(Sounds.TAP);

        finish();
        startActivity(new Intent(NoCameraAlertActivity.this, MainActivity.class));
    }

    @Override
    public CardBuilder buildAlertCard(Context context)
    {
        CardBuilder cardBuilder = new CardBuilder(context, CardBuilder.Layout.ALERT);
        cardBuilder.setIcon(getResources().getDrawable(R.drawable.ic_warning_150));
        cardBuilder.setText(R.string.msg_no_camera_found);
        cardBuilder.setFootnote(R.string.msg_tap_to_try_again);
        return cardBuilder;
    }
}
