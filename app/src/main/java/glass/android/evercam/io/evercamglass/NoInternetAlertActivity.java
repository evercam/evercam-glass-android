package glass.android.evercam.io.evercamglass;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.provider.Settings;

import com.google.android.glass.media.Sounds;
import com.google.android.glass.widget.CardBuilder;


public class NoInternetAlertActivity extends AlertActivity
{
    private final String TAG = "evercamglass-NoInternetAlertActivity";

    @Override
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    @Override
    void onGestureTapped()
    {
        AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audio.playSoundEffect(Sounds.TAP);
        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
    }

    @Override
    public CardBuilder buildAlertCard(Context context)
    {
        CardBuilder cardBuilder = new CardBuilder(context, CardBuilder.Layout.ALERT);
        cardBuilder.setIcon(getResources().getDrawable(R.drawable.ic_cloud_sad_150));
        cardBuilder.setText(R.string.msg_no_network);
        cardBuilder.setFootnote(R.string.msg_tap_to_view_network);
        return cardBuilder;
    }
}
